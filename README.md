# Wild_Adventures

Développez une application web de réservation de voyages pour l’agence « Wild Adventures »

Contexte:
      
      L’agence « Wild Adventures » organise des voyages d’aventure autour du globe pour les passionnés de sensations fortes : traversées de déserts, séjours dans les pôles, croisières sauvages, etc.

      Fort de son succès, l'agence a décidé d'ouvrir son catalogue au monde entier via une application web.

      Cette application devra :
          - Afficher les catégories d’aventures afin de permettre au client de choisir.
          - Lister toutes les aventures disponibles dans une catégorie : image, prix et dates.
          - Afficher chaque aventure dans une page avec : images, prix, date, descriptif, nombre de participants.
          - Mettre en oeuvre un système de commentaires de client pour chaque aventure, à afficher sur la page de celle-ci.
          - Permettre au client de s'inscrire et d'accéder à son espace contenant la liste de ses commandes.
          - Fournir la fonctionnalité de paiement de l’aventure à réserver.
    
Méthodologies de projet :
      
      Méthodologie Agile Scrum, gitFlow.
      
Technologies Backend:
    
    - Docker
    - Spring Data (JPA, Hibernet).
    - Spring Boot.
    - Junit 5.
    - Git.
    - Log4J.
    - Faign.
    - Spring Cloud Config.
    - Eureka.
    - Ribbon.
    - Zuul.
    - Zipkin.
    - Spring Sleuth.
    - Spring Actuator.
    - Spring Admin.
    
Livrables:
    
    - Le backlog de la release contenant la liste des US, l’estimation et l’affectation (document PDF)
    - Un dossier de conception contenant à minima le diagramme de composants et de déploiements (document PDF)
    - Les éléments de suivi de projet (comptes rendus de réunions, etc.)
    - Le support de présentation pour la soutenance (Diaporama, document PDF)

    Pour chaque microservice, vous livrerez également :
        - Le code source complet du microservice.
        - Un fichier SQL pour recréer la base de données avec des données de démo.
        - Un fichier Postman pour tester l’API exposée.
        - Une documentation complète de l’API automatiquement générée.
        - Le readme avec la procédure de mise en service à partir de zéro du microservice (comment configurer les accès à la BDD, comment configurer l’accès au fichier de configuration, le port d’écoute,  etc.).

